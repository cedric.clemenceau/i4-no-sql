import json
import uuid
import datetime
from django.http import HttpResponse
from django_redis import get_redis_connection

con = get_redis_connection("default")


def delete_get_notes(request, key):
    if request.method == 'GET':
        note = con.hget("listnotes", key)
        return HttpResponse(note.decode('utf-8') + '\n')
    elif request.method == 'DELETE':
        con.hdel("listnotes", key)
        return HttpResponse("Elément supprimé : \n" + key + '\n')
    else:
        return HttpResponse("Oups, mauvaise requête\n")


def get_all_set_notes(request):

    if request.method == 'GET':
        all_notes = con.hgetall("listnotes")
        data_to_print = {k.decode('utf-8'): v.decode('utf-8') for k, v in all_notes.items()}
        return HttpResponse(json.dumps(data_to_print))
    elif request.method == 'POST':
        data = {}
        ts = datetime.datetime.now().isoformat()
        key = uuid.uuid4().hex
        data['timestamp'] = ts
        data['auteur'] = 'Auteur1'
        data['data'] = request.body.decode('utf-8')
        con.hset("listnotes", key, str(data))
        return HttpResponse(key + '\n')
    else:
        return HttpResponse("Oups, mauvaise requête\n")
