from django.urls import path
from .views import *

urlpatterns = [
        path('notes/<str:key>/', delete_get_notes),
        path('notes/', get_all_set_notes),
]
