# Membres du projet :
- CLEMENCEAU Cedric
- SOUMAILLE Lucas

# Architecture :

C'est une application Django, donc fonctionnant avec python3.
L'application respecte le principe MVT (Model / View / Template). 
Nous avons le controleur qui est représenté par les fichiers urls.py et les fonctionnalités dans api/views.py. 
On utilise un redis installé sur la machine au préalable sur 127.0.0.1:6379, vous pouvez modifier ce dernier dans les settings django.
Il a été décidé de construire l'uri de la manière à respecter les normes des api restful (exemple :http://api.exemple.com/collection/) donc avec un / a la fin des collections.

# Utilisation :

### Installez git avec la commande (pour ubuntu par exemple) : 
$ sudo apt-get install git
### Il faut ensuite récupérer le répertoire sur le serveur gitlab : 
$ git clone https://gitlab.com/cedric.clemenceau/i4-no-sql.git
### Allez dans le répertoire tp_redis avec la commande :
$ cd tp3_redis
### Pensez à créer ou utiliser un environnement virtuel sous python3 et à installer les requirements avec la commande :
$ pip install -r requirements.txt
### Une fois le redis lancé selon les recommandations ci-dessus, on lance le serveur avec la commande :
$ python manage.py runserver [ip:port]

### Vous pouvez ensuite utiliser n'importe client pour envoyer des commandes:
- Get sans aucune donnee :
	- http://{my_ip_address:port}/notes/ - retourne toutes les notes (key, value)
	- http://{my_ip_address:port}/notes/{id_note}/ - retourne le contenu d'une note

- Post avec les donnees dans le body (sinon ne marchera pas):
	- http://{my_ip_address:port}/notes/ - créer une nouvelle note, return l'uuid

- Delete sans donnee :
	- http://{my_ip_address:port}/notes/{id_notes}/ - supprime une note


### Exemple en curl:
 - POST : curl -H "Content-Type:text/plain" --data 'Ceci est un test' http://127.0.0.1:8000/notes/
 - GET :
	- all : curl -X GET  http://127.0.0.1:8000/notes/
	- unique : curl -X GET  http://127.0.0.1:8000/notes/{id_note}/
 - DELETE : curl -X DELETE http://127.0.0.1:8000/notes/{id_note}/

